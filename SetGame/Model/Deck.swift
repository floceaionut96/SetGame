//
//  Deck.swift
//  SetGame
//
//  Created by Ionut Flocea on 23.02.2022.
//

import Foundation
import UIKit

struct Deck {
    
    private var deck: [Card] = []
    private(set) var cardsInGame: [Card] = []
    private var cardsOnTable = 12
    private(set) var cardsPressed: [Card] = []
    private(set) var next3Cards: [Card] = []
    var buttonsAndCards: [Int:Card] = [:]

    init() {
        for number in NumberOfShapes.allCases {
            for shape in Shape.allCases {
                for color in Color.allCases {
                    for shade in Shade.allCases {
                        let card = Card(shape: shape, shade: shade, noOfShapes: number, color: color)
                        deck.append(card)
                    }
                }
            }
        }
        deck.shuffle()
        addFirstCardsInGame()
        setButtonsAndCards()
        next3Cards = getNextAvailableCards()
    }
    
    func getNextAvailableCards() -> [Card] {
        var cards: [Card] = []
        for card in deck {
            if card.isAvailable {
                cards.append(card)
                card.setNotAvailable()
            }
            if cards.count == 3{
                return cards
            }
        }
        return cards
    }
    
    func checkIfSet(cards: [Card]) -> Bool{
        return (cards[0].noOfShapes.rawValue + cards[1].noOfShapes.rawValue + cards[2].noOfShapes.rawValue) % 3 == 0
                && (cards[0].shape.rawValue  + cards[1].shape.rawValue  + cards[2].shape.rawValue ) % 3 == 0
                && (cards[0].shade.rawValue  + cards[1].shade.rawValue  + cards[2].shade.rawValue ) % 3 == 0
                && (cards[0].color.rawValue  + cards[1].color.rawValue  + cards[2].color.rawValue ) % 3 == 0
    }
    
    mutating func addFirstCardsInGame(){
        for index in 0...11{
            cardsInGame.append(deck[index])
            deck[index].setNotAvailable()
        }
    }
    
    mutating func addInCardsPressed(card: Card){
        cardsPressed.append(card)
    }
    
    func cardOnIndex(index: Int) -> Card {
        return deck[index]
    }
    
    mutating func removeCardPressed(card: Card){
        cardsPressed.removeAll(where: { $0 == card})
    }
    
    mutating func replaceCardsIfSet(){
        for i in 0...2{
            replaceCard(card1: cardsPressed[i], card2: next3Cards[i])
        }
        next3Cards = getNextAvailableCards()
    }
    
    func resetButtonsStatus(){
        for card in cardsPressed{
            card.setIsPressedStatus(status: false)
        }
    }
    
    mutating func removeCardsPressed(){
        resetButtonsStatus()
        cardsPressed.removeAll()
    }

    
    func setCardsUnavailable(cards: [Card]){
        for card in cards {
            card.setNotAvailable()
        }
    }
    
    mutating func replaceCard(card1: Card, card2: Card){
        if let key = buttonsAndCards.someKey(forValue: card1){
            buttonsAndCards[key] = card2
        }
    }
    
    mutating func setButtonsAndCards(){
        for i in 0...11{
            buttonsAndCards[i] = cardsInGame[i]
        }
    }
    
   func getButtonIndex(card: Card) ->Int {
       if let key = buttonsAndCards.someKey(forValue: card){
           return key}
       return -1
    }
    
}

extension Dictionary where Value: Equatable {
    func someKey(forValue val: Value) -> Key? {
        return first(where: { $1 == val })?.key
    }
}

