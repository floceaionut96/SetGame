//
//  NumberOfShapes.swift
//  SetGame
//
//  Created by Ionut Flocea on 24.02.2022.
//

import Foundation
enum NumberOfShapes: Int, CaseIterable {
    case one = 0
    case two = 1
    case three = 2
    
    func noOfShapes() -> Int {
        switch self {
        case .one:
            return 1
        case .two:
            return 2
        case .three:
            return 3
        }
    }
}
