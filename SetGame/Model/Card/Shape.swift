//
//  Shape.swift
//  SetGame
//
//  Created by Ionut Flocea on 24.02.2022.
//

import Foundation

enum Shape: Int, CaseIterable {
    case circle = 0
    case square = 1
    case triangle = 2
    
    func shape() -> String {
        switch self {
        case .circle:
            return "●"
        case .square:
            return "■"
        case .triangle:
            return "▲"
        }
    }
}
