//
//  Card.swift
//  SetGame
//
//  Created by Ionut Flocea on 23.02.2022.
//

import Foundation
import UIKit

class Card: Equatable {
    
    private(set) var shape: Shape
    private(set) var shade: Shade
    private(set) var noOfShapes: NumberOfShapes
    private(set) var color: Color
    private(set) var isPressed: Bool
    private(set) var isAvailable: Bool
    static var IDfactory = 0
    var ID: Int
    var description: String {
        return "Card:\(self.shape), \(self.shade), \(self.noOfShapes), \(self.color)"
        }
    
    init(shape: Shape, shade: Shade, noOfShapes: NumberOfShapes, color: Color) {
        self.shape=shape
        self.shade=shade
        self.noOfShapes=noOfShapes
        self.color=color
        self.isPressed = false
        self.isAvailable = true
        self.ID = Card.getUniqueID()
    }
    
    static func getUniqueID() -> Int {
        Card.IDfactory+=1
        return Card.IDfactory
    }
    
    func changeIsPressedStatus() {
        switch isPressed{
        case true: isPressed = false
        case false: isPressed = true
        }
    }
    
    func setIsPressedStatus(status: Bool) {
        self.isPressed = status
    }
    
    func setNotAvailable(){
        self.isAvailable = false
    }
    
    
    static func == (lhs: Card, rhs: Card) -> Bool {
        return (lhs.shape.rawValue == rhs.shape.rawValue) &&
        (lhs.color.rawValue == rhs.color.rawValue) &&
        (lhs.shade.rawValue == rhs.shade.rawValue) &&
        (lhs.noOfShapes.rawValue == rhs.noOfShapes.rawValue)
    }

}
