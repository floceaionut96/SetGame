//
//  Shade.swift
//  SetGame
//
//  Created by Ionut Flocea on 23.02.2022.
//

import Foundation
enum Shade: Int, CaseIterable {
    case solid = 0
    case striped = 1
    case open = 2
    
}
