//
//  ViewController.swift
//  SetGame
//
//  Created by Ionut Flocea on 23.02.2022.
//

import UIKit

class ViewController: UIViewController {

    var gameDeck = Deck()
    
    @IBOutlet var cardButtons: [UIButton]!
    @IBOutlet weak var btt1: UIButton!
    @IBAction func printInConsole(_ sender: UIButton) {
        
        
        print("Cards pressed now:\(gameDeck.cardsPressed.count)")
        for index in gameDeck.cardsPressed.indices{
            
            print("\(index):\(gameDeck.cardsPressed[index].ID)")
        }
        
        print("Next Cards:\(gameDeck.next3Cards.count)")
        for index in gameDeck.next3Cards.indices{
            
            print("\(index):\(gameDeck.next3Cards[index].ID)")
        }
        
        print("Dictionary")
        for i in 0...11{
            print("button[\(i)] = \(gameDeck.buttonsAndCards[i]!.ID)")
        }
        
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        designButtons(buttons: cardButtons)
        loadButtonsFaces()
        
    }

    
    @IBAction func cardPressed(_ sender: UIButton) {
        
        if gameDeck.cardsPressed.count == 3 {
        
                    gameDeck.removeCardsPressed()
                    updateButtons()
                    unlockButtons()
                    loadButtonsFaces()
                }
        
        gameDeck.buttonsAndCards[sender.tag]!.changeIsPressedStatus()
        
        if gameDeck.buttonsAndCards[sender.tag]!.isPressed == true {
            highlightButton(button: sender, highlight: true)
            gameDeck.addInCardsPressed(card: gameDeck.buttonsAndCards[sender.tag]!)
        } else {
            highlightButton(button: sender, highlight: false)
            gameDeck.removeCardPressed(card: gameDeck.buttonsAndCards[sender.tag]!)
        }
        
        if gameDeck.cardsPressed.count == 3 {
            print("Cards pressed now in if:\(gameDeck.cardsPressed.count)")
            for index in gameDeck.cardsPressed.indices{
                
                print("\(index):\(gameDeck.cardsPressed[index].ID)")
            }
            lockPressedButtons(pressedCards: gameDeck.cardsPressed)
            let isSet = gameDeck.checkIfSet(cards: gameDeck.cardsPressed)
            for button in cardButtons{
                       if button.layer.borderWidth == 4 {
                           if isSet == true{
                               button.layer.borderWidth = 4
                               button.layer.borderColor = UIColor.green.cgColor
                           } else {
                               button.layer.borderWidth = 4
                               button.layer.borderColor = UIColor.red.cgColor
                           }
                       }
                   }
            if isSet{
                            print("FOUND SET")
                            gameDeck.replaceCardsIfSet()
                   
            }
        
        }
        
    }
    

    func createCardFace(card: Card) -> NSAttributedString {
        return NSAttributedString(
            string: createCardText(card: card),
            attributes: createCardAttributes(card: card)
        );
    }
    
    func createCardText(card: Card) -> String {
        switch card.noOfShapes{
        case .one:
            return card.shape.shape()
        case .two:
            return (card.shape.shape() + card.shape.shape())
        case .three:
            return (card.shape.shape() + card.shape.shape() + card.shape.shape())
        }
    }
    
    func createCardAttributes(card: Card) -> [NSAttributedString.Key: Any]{
        switch card.shade{
        case .solid:
            return [
                NSAttributedString.Key.foregroundColor: card.color.uiColor()
            ]
        case .striped:
            return [
                NSAttributedString.Key.strokeColor: card.color.uiColor(),
                NSAttributedString.Key.foregroundColor: card.color.uiColor().withAlphaComponent(0.2),
                NSAttributedString.Key.strokeWidth: -7.0
            ]
        case .open:
            return [
                NSAttributedString.Key.strokeColor: card.color.uiColor(),
                NSAttributedString.Key.foregroundColor: UIColor.white,
                NSAttributedString.Key.strokeWidth: -7.0
            ]
        }
    }
    
    func updateButtons(){
        for button in cardButtons{
            highlightButton(button: button, highlight: false)
        }
    }
    
    func highlightButton(button: UIButton, highlight: Bool){
        if highlight{
        button.layer.borderWidth = 4
        button.layer.borderColor = UIColor.init(red: 0, green: 255, blue: 255, alpha: 0.7).cgColor
        } else {
            button.layer.borderWidth = 0
        }
    }
    
    
    func designButtons(buttons: [UIButton]){
        for button in buttons {
            button.backgroundColor = UIColor(red: 255.0/255, green: 238.0/255, blue: 187.0/255, alpha: 0.7)
            button.layer.cornerRadius = 10
            button.layer.borderWidth = 1
        }
    }
    
    func lockPressedButtons(pressedCards: [Card]){
        if pressedCards.count == 3 {
            for card in gameDeck.cardsPressed {
                cardButtons[gameDeck.getButtonIndex(card: card)].isEnabled = false
            }
        }
    }
    
    func unlockButtons(){
        for button in cardButtons{
            button.isEnabled = true
        }
    }
    
    func loadButtonsFaces(){
        for index in 0...11{
            cardButtons[index].setAttributedTitle(createCardFace(card: gameDeck.buttonsAndCards[index]!), for: .normal)
    }
    }

}

